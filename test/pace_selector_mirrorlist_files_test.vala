/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;
using IniParser;

class PaceSelectorMirrorlistFilesTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceSelectorMirrorlistFilesTest.add_tests ();

    string file_content = "Server = value\n" +
                          "#Server = value2\n";

    try {
      FileUtils.set_contents ( "pace_selector_mirrorlist_files", file_content );
    } catch ( Error e ) {
      stderr.printf ( "Error creating mirrorlist file (%s)\n", e.message );
    }

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/selector_mirrorlist_files", () => {
      SelectorMirrorlistFiles test_selector = new SelectorMirrorlistFiles ();

      assert ( test_selector is SelectorMirrorlistFiles );
    } );

    Test.add_func ( "/pace/selector_mirrorlist_files/set_mirrorlist_files/get_current_mirrorlist_file", () => {
      bool check = true;

      SelectorMirrorlistFiles test_selector = new SelectorMirrorlistFiles ();
      Array<Mirrorlist> test_mirrorlists = new Array<Mirrorlist> ();

      test_mirrorlists.append_val ( new Mirrorlist ( "pace_selector_mirrorlist_files" ) );

      test_selector.set_mirrorlist_files ( test_mirrorlists );

      Array<Key> mirrors = test_selector.get_current_mirrorlist_file ().get_no_section_keys ();

      check = check && mirrors.index ( 0 ).to_string () == "Server = value\n";
      check = check && mirrors.index ( 1 ).to_string () == "# Server = value2\n";

      assert ( check );
    } );
  }
}
