/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;

class PaceRowLabelEntryTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceRowLabelEntryTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/row_label_entry", () => {
      RowLabelEntry test_row = new RowLabelEntry ( "key" );

      assert ( test_row is RowLabelEntry );
    } );

    Test.add_func ( "/pace/row_label_entry/get_value/unassigned", () => {
      RowLabelEntry test_row = new RowLabelEntry ( "key" );

      assert ( test_row.get_value () == "" );
    } );

    Test.add_func ( "/pace/row_label_entry/get_value/constructor", () => {
      RowLabelEntry test_row = new RowLabelEntry ( "key", "value" );

      assert ( test_row.get_value () == "value" );
    } );

    Test.add_func ( "/pace/row_label_entry/set_value", () => {
      RowLabelEntry test_row = new RowLabelEntry ( "key" );

      test_row.set_value ( "value" );

      assert ( test_row.get_value () == "value" );
    } );
  }
}
