/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;

class PaceListBoxFixedTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceListBoxFixedTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/listboxfixed", () => {
      ListBoxFixed test_listbox = new ListBoxFixed ();

      assert ( test_listbox is ListBoxFixed );
    } );

    Test.add_func ( "/pace/listbox/insert_row_label_entry", () => {
      ListBoxFixed test_listbox = new ListBoxFixed ();

      test_listbox.insert_row_label_entry ( "entry", -1, "value" );

      var row = test_listbox.get_row_at_index ( 0 ).get_child () as RowKeyValue;
      bool result = row.get_key () == "entry";
      result = result && (row.get_value () == "value");

      assert ( result );
    } );

    Test.add_func ( "/pace/listbox/insert_row_combobox_entry", () => {
      ListBoxFixed test_listbox = new ListBoxFixed ();

      test_listbox.insert_row_combobox_entry ( "Server", -1, "value" );

      var row = test_listbox.get_row_at_index ( 0 ).get_child () as RowKeyValue;
      bool result = row.get_key () == "Server";
      result = result && (row.get_value () == "value");

      assert ( result );
    } );

    Test.add_func ( "/pace/listbox/insert_row_label_label", () => {
      ListBoxFixed test_listbox = new ListBoxFixed ();

      test_listbox.insert_row_label_label ( "label", -1, "value" );

      var row = test_listbox.get_row_at_index ( 0 ).get_child () as RowKeyValue;
      bool result = row.get_key () == "label";
      result = result && (row.get_value () == "value");

      assert ( result );
    } );
  }
}
