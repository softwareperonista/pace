/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;
using IniParser;

class PaceListBoxRepositoriesTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceListBoxRepositoriesTest.add_tests ();

    string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                          "[repo1]\nServer = value\n\n" +
                          "#[repo2]\n#Server = value\n#Include = value2";

    try {
      FileUtils.set_contents ( "pace_listbox_repositories_test.conf", file_content );
    } catch ( Error e ) {
      stderr.printf ( "Error creating config file (%s)\n", e.message );
    }

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/listbox_repositories", () => {
      ListBoxRepositories test_listbox = new ListBoxRepositories ();

      assert ( test_listbox is ListBoxRepositories );
    } );

    Test.add_func ( "/pace/listbox_repositories/set_repositories/get_repositories", () => {
      bool check = true;

      ListBoxRepositories test_listbox = new ListBoxRepositories ();

      ConfigFile test_file = new ConfigFile ( "pace_listbox_repositories_test.conf" );

      test_listbox.set_repositories ( test_file );

      Array<Section> repositories = test_file.get_repositories ();

      check = check && repositories.index ( 0 ).to_string () == "[repo1]\nServer = value\n";
      check = check && repositories.index ( 1 ).to_string () == "# [repo2]\n# Server = value\n# Include = value2\n";

      assert ( check );
    } );
  }
}
