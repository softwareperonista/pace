/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;

class PaceListBoxTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceListBoxTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/listbox", () => {
      ListBox test_listbox = new ListBox ();

      assert ( test_listbox is ListBox );
    } );

    Test.add_func ( "/pace/listbox/insert/length", () => {
      RowLabelEntry test_entry = new RowLabelEntry ( "key", "value" );
      RowLabelEntry test_entry2 = new RowLabelEntry ( "key", "value" );

      ListBox test_listbox = new ListBox ();

      test_listbox.insert ( test_entry, -1 );
      test_listbox.insert ( test_entry2, -1 );

      assert ( test_listbox.length == 2 );
    } );

    Test.add_func ( "/pace/listbox/insert/order", () => {
      RowLabelEntry test_entry = new RowLabelEntry ( "key", "value" );
      RowLabelEntry test_entry2 = new RowLabelEntry ( "key2", "value2" );

      ListBox test_listbox = new ListBox ();

      test_listbox.insert ( test_entry, -1 );
      test_listbox.insert ( test_entry2, 0 );

      var row = test_listbox.get_row_at_index ( 0 ).get_child () as RowKeyValue;
      bool result = row.get_key () == "key2";
      result = result && (row.get_value () == "value2");

      row = test_listbox.get_row_at_index ( 1 ).get_child () as RowKeyValue;
      result = result && (row.get_key () == "key");
      result = result && (row.get_value () == "value");

      assert ( result );
    } );

    Test.add_func ( "/pace/listbox/clear", () => {
      ListBox test_listbox = new ListBox ();

      RowLabelEntry test_entry = new RowLabelEntry ( "key", "value" );
      test_listbox.insert ( test_entry, -1 );

      test_listbox.clear ();

      assert ( test_listbox.get_row_at_index ( 0 ) == null );
    } );

    Test.add_func ( "/pace/listbox/clear/length", () => {
      ListBox test_listbox = new ListBox ();

      RowLabelEntry test_entry = new RowLabelEntry ( "key", "value" );
      test_listbox.insert ( test_entry, -1 );

      test_listbox.clear ();

      assert ( test_listbox.length == 0 );
    } );
  }
}
