/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;
using IniParser;

class PaceListBoxMirrorsTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceListBoxMirrorsTest.add_tests ();

    string file_content = "Server = value\n" +
                          "#Server = value2\n";

    try {
      FileUtils.set_contents ( "pace_listbox_mirrors_test.conf", file_content );
    } catch ( Error e ) {
      stderr.printf ( "Error creating mirrorlist file (%s)\n", e.message );
    }

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/listbox_mirrors", () => {
      ListBoxMirrors test_listbox = new ListBoxMirrors ( "pace_listbox_mirrors_test.conf" );

      assert ( test_listbox is ListBoxMirrors );
    } );

    Test.add_func ( "/pace/listbox_options/set_mirrors/get_mirrors", () => {
      bool check = true;

      ListBoxMirrors test_listbox = new ListBoxMirrors ( "pace_listbox_mirrors_test.conf" );

      Array<Key> mirrors = test_listbox.get_mirrorlist_file ().get_no_section_keys ();

      check = check && mirrors.index ( 0 ).to_string () == "Server = value\n";
      check = check && mirrors.index ( 1 ).to_string () == "# Server = value2\n";

      assert ( check );
    } );
  }
}
