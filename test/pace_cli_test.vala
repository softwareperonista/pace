/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;

class PaceCliTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    PaceCliTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/cli", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman.conf";

      PaceCliTest.create_file ( file_name, file_content );

      Cli test_cli = new Cli ( file_name );

      assert ( test_cli is Cli );
    } );

    Test.add_func ( "/pace/cli/list", () => {
      FileStream stdout_temp = (owned)stdout;
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_list.conf";
      PaceCliTest.create_file ( file_name, file_content );
      string[] args = {"", "list"};


      Cli test_cli_list = new Cli ( file_name );

      stdout = FileStream.open ( "pace-cli-list.stdout", "w" );
      test_cli_list.run ( args );
      stdout = (owned)stdout_temp;

      string pace_cli_list_stdout = PaceCliTest.read_file ( "pace-cli-list.stdout" );

      string expected_string = "repo1\nrepo2     (disabled)\n";

      assert ( pace_cli_list_stdout == expected_string );
    } );

    Test.add_func ( "/pace/cli/list_options", () => {
      FileStream stdout_temp = (owned)stdout;
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_list_options.conf";
      PaceCliTest.create_file ( file_name, file_content );
      string[] args = {"", "list-options"};


      Cli test_cli_list_options = new Cli ( file_name );

      stdout = FileStream.open ( "pace-cli-list_options.stdout", "w" );
      test_cli_list_options.run ( args );
      stdout = (owned)stdout_temp;

      string pace_cli_list_options_stdout = PaceCliTest.read_file ( "pace-cli-list_options.stdout" );

      string expected_string = "key        (no setted)\nkey2       value2\noption\n";

      assert ( pace_cli_list_options_stdout == expected_string );
    } );

    Test.add_func ( "/pace/cli/enable", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_enable.conf";
      PaceCliTest.create_file ( file_name, file_content );
      string[] args = {"", "enable", "repo2"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "[repo2]\nServer = value\nInclude = value2\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/remove", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_remove.conf";
      PaceCliTest.create_file ( file_name, file_content );
      string[] args = {"", "remove", "repo2"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/disable", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_disable.conf";
      PaceCliTest.create_file ( file_name, file_content );
      string[] args = {"", "disable", "repo1"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "# [repo1]\n# Server = value\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/set/key/commented", () => {
      string file_content = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "# [repo2]\n# Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_set.conf";
      PaceCliTest.create_file ( file_name, file_content );
      string[] args = {"", "set", "key", "test_value"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\nkey = test_value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/set/key/uncommented", () => {
      string file_content = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_set.conf";
      PaceCliTest.create_file ( file_name, file_content );
      string[] args = {"", "set", "key2", "test_value"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = test_value\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/set/option", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\n#option\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_set.conf";
      PaceCliTest.create_file ( file_name, file_content );
      string[] args = {"", "set", "option"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/unset", () => {
      string file_content = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "# [repo2]\n# Server = value\n# Include = value2";
      string file_name = "cli_test_pacman_unset.conf";
      PaceCliTest.create_file ( file_name, file_content );
      string[] args = {"", "unset", "key2"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\n# key2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/unset/option", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_unset_option.conf";
      PaceCliTest.create_file ( file_name, file_content );
      string[] args = {"", "unset", "option"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\n# option\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n";

      assert ( file_changed == expected_string );
    } );
  }

  private static void create_file ( string name, string content ) {
    try {
      FileUtils.set_contents ( name, content );
    } catch ( Error e ) {
      stderr.printf ( "Error creating config file (%s)\n", e.message );
    }
  }

  private static string read_file ( string file_name ) {
    string file_content = "";
    try {
      FileUtils.get_contents ( file_name, out file_content );
    } catch ( Error e ) {
      stderr.printf ( "Error opening config file (%s)\n", e.message );
    }

    return file_content;
  }
}
