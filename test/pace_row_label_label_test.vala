/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;

class PaceRowLabelLabelTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceRowLabelLabelTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/row_label_label", () => {
      RowLabelLabel test_row = new RowLabelLabel ( "key" );

      assert ( test_row is RowLabelLabel );
    } );

    Test.add_func ( "/pace/row_label_label/get_value/unassigned", () => {
      RowLabelLabel test_row = new RowLabelLabel ( "key" );

      assert ( test_row.get_value () == "" );
    } );

    Test.add_func ( "/pace/row_label_label/get_value/constructor", () => {
      RowLabelLabel test_row = new RowLabelLabel ( "key", "value" );

      assert ( test_row.get_value () == "value" );
    } );

    Test.add_func ( "/pace/row_label_label/set_value", () => {
      RowLabelLabel test_row = new RowLabelLabel ( "key" );

      test_row.set_value ( "value" );

      assert ( test_row.get_value () == "value" );
    } );
  }
}
