/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;
using IniParser;

class PaceRowOptionTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceRowOptionTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/row_option", () => {
      Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.NO );

      RowOption test_row = new RowOption ( test_key );

      assert ( test_row is RowOption );
    } );

    Test.add_func ( "/pace/row_option/to_config_key", () => {
      Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.NO );

      RowOption test_row = new RowOption ( test_key );

      Key row_returned_key = test_row.to_config_key ();

      debug ( "test_key: " + test_key.to_string () );
      debug ( "row_returned_key: " + row_returned_key.to_string () );
      assert ( row_returned_key.to_string () == test_key.to_string () );
    } );
  }
}
