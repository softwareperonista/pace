/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;
using IniParser;

class PaceRowRepositoryTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceRowRepositoryTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/listboxrow_repository", () => {
      RowRepository test_row = new RowRepository ();

      assert ( test_row is RowRepository );
    } );

    Test.add_func ( "/pace/listboxrow_repository/name", () => {
      RowRepository test_row = new RowRepository ();

      test_row.set_name ( "test" );
      Section row_returned_repository = test_row.to_config_repository ();

      assert ( row_returned_repository.to_string () == "# [test]\n" );
    } );

    Test.add_func ( "/pace/listboxrow_repository/name/enabled", () => {
      RowRepository test_row = new RowRepository ();

      test_row.set_name ( "test" );
      test_row.set_enabled ( true );
      Section row_returned_repository = test_row.to_config_repository ();

      assert ( row_returned_repository.to_string () == "[test]\n" );
    } );

    Test.add_func ( "/pace/listboxrow_repository/set_key", () => {
      Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.NO );

      RowRepository test_row = new RowRepository ();

      test_row.set_name ( "test" );
      test_row.set_key ( test_key );
      Section row_returned_repository = test_row.to_config_repository ();

      assert ( row_returned_repository.to_string () == "# [test]\n# key = value\n" );
    } );

    Test.add_func ( "/pace/listboxrow_repository/set_key/enable", () => {
      Key test_key = new Key ( "key", "value", KeyWithValue.YES, Commented.NO );

      RowRepository test_row = new RowRepository ();

      test_row.set_name ( "test" );
      test_row.set_key ( test_key );
      test_row.set_enabled ( true );
      Section row_returned_repository = test_row.to_config_repository ();

      assert ( row_returned_repository.to_string () == "[test]\nkey = value\n" );
    } );

    Test.add_func ( "/pace/listboxrow_repository/set_keys", () => {
      RowLabelEntry test_entry = new RowLabelEntry ( "Name", "test-repo" );
      RowComboBoxEntry test_entry2 = new RowComboBoxEntry ( "Server", "value2" );
      RowComboBoxEntry test_entry3 = new RowComboBoxEntry ( "Include", "value3" );

      ListBox test_listbox = new ListBox ();

      test_listbox.insert ( test_entry, -1 );
      test_listbox.insert ( test_entry2, -1 );
      test_listbox.insert ( test_entry3, -1 );

      RowRepository test_row = new RowRepository ();

      test_row.set_keys ( test_listbox );
      Section row_returned_repository = test_row.to_config_repository ();

      assert ( row_returned_repository.to_string () == "# [test-repo]\n# Server = value2\n# Include = value3\n" );
    } );

    Test.add_func ( "/pace/listboxrow_repository/set_keys/enable", () => {
      RowLabelEntry test_entry = new RowLabelEntry ( "Name", "test-repo" );
      RowComboBoxEntry test_entry2 = new RowComboBoxEntry ( "Server", "value2" );
      RowComboBoxEntry test_entry3 = new RowComboBoxEntry ( "Include", "value3" );

      ListBox test_listbox = new ListBox ();

      test_listbox.insert ( test_entry, -1 );
      test_listbox.insert ( test_entry2, -1 );
      test_listbox.insert ( test_entry3, -1 );

      RowRepository test_row = new RowRepository ();

      test_row.set_keys ( test_listbox );
      test_row.set_enabled ( true );
      Section row_returned_repository = test_row.to_config_repository ();

      assert ( row_returned_repository.to_string () == "[test-repo]\nServer = value2\nInclude = value3\n" );
    } );
  }
}
