/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_listbox_sortable.vala
 *
 * Copyright 2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Pace {
  class ListBoxSortable : ListBox {
    private Gtk.ListBoxRow previous_target_row;
    private Gtk.ListBoxRow dragged_row;

    construct {
      this.previous_target_row = null;

      Gtk.TargetEntry[] entries = {
        { "GTK_LIST_BOX_ROW", Gtk.TargetFlags.SAME_APP, 0}
      };

      Gtk.drag_dest_set (
        this.listbox,
        Gtk.DestDefaults.ALL,
        entries,
        Gdk.DragAction.MOVE
        );

      this.listbox.drag_motion.connect ( this.on_drag_motion );
      this.listbox.drag_data_received.connect ( this.on_drag_data_received );
      this.listbox.drag_leave.connect ( this.on_drag_leave );
    }

    public new void insert ( Gtk.Widget widget, int position ) {
      base.insert ( widget, position );

      if ( position == -1 ) {
        position = this.length -1;
      }

      var row = this.listbox.get_row_at_index ( position ).get_child () as RowSortable;
      row.drag_start.connect ( this.on_drag_start );
      row.drag_over.connect ( this.on_drag_over );
    }

    private void move ( Gtk.ListBoxRow row, int target_position ) {
      var target_row = this.listbox.get_row_at_index ( target_position );

      if ( target_row != row ) {
        this.remove_listboxrow ( row );
        row.unparent ();
        target_position = target_row.get_index ();
        base.insert ( row, target_position );
        this.changed ();
      }
    }

    private void on_drag_start ( Gtk.ListBoxRow row ) {
      this.dragged_row = row;
    }

    private void on_drag_data_received ( Gdk.DragContext context, int x, int y, Gtk.SelectionData selection_data, uint info, uint time_ ) {
      if ( this.dragged_row != null ) {
        var target_row = this.listbox.get_row_at_y ( y );

        Gtk.ListBoxRow orig_row = ( (Gtk.Widget[])selection_data.get_data () )[0] as Gtk.ListBoxRow;

        if ( orig_row != target_row ) {
          var target_position = this.length;
          if ( target_row != null ) {
            target_position = target_row.get_index ();
          }

          int prev = 0;
          if ( y > this.target_row_middle ( target_row ) ) {
            prev = 1;
          }

          this.move ( orig_row, target_position + prev );

          this.dragged_row = null;
          this.previous_target_row = null;
        }

        if ( this.previous_target_row != null ) {
          this.previous_target_row.set_margin_top ( 0 );
          this.previous_target_row.set_margin_bottom ( 0 );
        }
      }
    }

    private bool on_drag_motion ( Gdk.DragContext context, int x, int y, uint time_ ) {
      var target_row = this.listbox.get_row_at_y ( y );

      if ( target_row != this.previous_target_row ) {
        if ( this.previous_target_row != null ) {
          this.previous_target_row.set_margin_top ( 0 );
          this.previous_target_row.set_margin_bottom ( 0 );
        }
        this.previous_target_row = target_row;
      }

      if ( target_row != null ) {
        if ( this.dragged_row != null ) {
          var margin = this.get_dragged_row_height ();

          if ( y < this.target_row_middle ( target_row ) ) {
            target_row.set_margin_top ( margin );
            target_row.set_margin_bottom ( 0 );
          } else {
            target_row.set_margin_top ( 0 );
            target_row.set_margin_bottom ( margin );
          }
        }
      }

      return true;
    }

    private void on_drag_leave ( Gdk.DragContext context, uint time_ ) {
      if ( this.dragged_row != null ) {
        this.clear_row_margins ();
        var next = this.listbox.get_row_at_index ( this.dragged_row.get_index () + 1 );
        debug ( "this.dragged_row.get_index (): " + this.dragged_row.get_index ().to_string () );
        if ( next != null ) {
          next.set_margin_top ( this.get_dragged_row_height () );
          this.previous_target_row =  next;
        } else {
          var prev = this.listbox.get_row_at_index ( this.dragged_row.get_index () - 1 );
          if ( prev != null ) {
            prev.set_margin_bottom ( this.get_dragged_row_height () );
            this.previous_target_row =  prev;
          }
        }
      }
    }

    private void on_drag_over () {
      this.dragged_row = null;
      this.previous_target_row = null;
      this.clear_row_margins ();
    }

    private int get_dragged_row_height () {
      var height = 0;
      if ( this.dragged_row != null ) {
        var alloc = Gtk.Allocation ();
        this.dragged_row.get_allocation ( out alloc );

        height = alloc.height;
      }

      return height;
    }

    public void clear_row_margins () {
      int i = 0;
      var row = this.listbox.get_row_at_index ( i );

      while ( row != null ) {
        row.set_margin_top ( 0 );
        row.set_margin_bottom ( 0 );

        i++;
        row = this.listbox.get_row_at_index ( i );
      }
    }

    private int target_row_middle ( Gtk.ListBoxRow target_row ) {
      var alloc = Gtk.Allocation ();
      target_row.get_allocation ( out alloc );

      return (alloc.y - target_row.get_margin_top () + (alloc.height + target_row.get_margin_top () + target_row.get_margin_bottom () )/2);
    }
  }
}
