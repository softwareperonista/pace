/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_config_file.vala
 *
 * Copyright (C) 2018-2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using IniParser;

namespace Pace {
  public class ConfigFile : Object {
    private IniFile file;
    private string original_content;

    public ConfigFile ( string file ) {
      this.file = new IniFile ( file );
      this.original_content = this.to_string ();

      this.remove_common_wrong_detections ();
    }

    public ConfigFile.mirrorlist ( string file ) {
      this.file = new IniFile.no_comments ( file );
      this.original_content = this.to_string ();

      this.remove_common_wrong_detections ();
    }

    private void remove_common_wrong_detections () {
      Array<Section> repositories = this.file.get_sections ();
      for ( int i = 0; i < repositories.length; i++ ) {
        if ( repositories.index ( i ).get_name () == "repo-name" ) {
          repositories.remove_index ( i );
        }
      }

      Array<Key> options = this.get_options ();
      for ( int i = 0; i < options.length; i++ ) {
        if ( options.index ( i ).get_key () == "REPOSITORIES" ) {
          options.remove_index ( i );
        }
      }
    }

    public Array<Section> get_repositories () {
      Array<Section> repositories = new Array<Section> ();

      for ( int i = 0; i < this.file.get_sections ().length; i++ ) {
        if ( this.file.get_sections ().index ( i ).get_name () != "options" ) {
          repositories.append_val ( this.file.get_sections ().index ( i ) );
        }
      }

      return repositories;
    }

    public Array<Key> get_options () {
      Section options = new Section ( "" );
      if ( this.file.get_section ( "options" ) != null ) {
        options = this.file.get_section ( "options" );
      }

      return options.get_keys ();
    }

    public Array<Key> get_no_section_keys () {
      return this.file.get_no_section_keys ();
    }

    public void set_no_section_keys ( Array<Key> keys ) {
      this.file.set_no_section_keys ( keys );
    }

    public Array<Mirrorlist> get_mirrorlists () {
      var mirrorlists = new Array<Mirrorlist> ();
      var repositories = this.get_repositories ();

      for ( int i = 0; i < repositories.length; i++ ) {
        var mirrorlist_files = this.get_mirrorlists_form_repository ( repositories.index ( i ) );

        for ( int j = 0; j < mirrorlist_files.length; j++ ) {
          if ( !(this.duplicated_mirrorlist_files ( mirrorlists, mirrorlist_files.index ( j ).get_path () ) ) ) {
            mirrorlists.append_val ( mirrorlist_files.index ( j ) );
          }
        }
      }

      return mirrorlists;
    }

    public Array<Mirrorlist> get_mirrorlists_form_repository ( Section repository ) {
      Array<Mirrorlist> mirrorlists = new Array<Mirrorlist> ();
      var keys = repository.get_keys ();

      for ( int i= 0; i < keys.length; i++ ) {

        if ( keys.index ( i ).get_key () == "Include" ) {
          var file = File.new_for_path ( keys.index ( i ).get_value () );
          if ( file.query_exists () ) {
            mirrorlists.append_val ( new Mirrorlist ( keys.index ( i ).get_value () ) );
          }
        }
      }

      return mirrorlists;
    }

    private bool duplicated_mirrorlist_files ( Array<Mirrorlist> mirrorlists, string path ) {
      bool return_value = false;

      for ( int i = 0; i < mirrorlists.length; i++ ) {
        if ( mirrorlists.index ( i ).get_path () == path ) {
          return_value = true;
          break;
        }
      }

      return return_value;
    }

    public void set_repositories ( Array<Section> repositories ) {
      if ( this.file.get_section ( "options" ) != null ) {
        repositories.prepend_val ( this.file.get_section ( "options" ) );
      }

      this.file.set_sections ( repositories );
    }

    public void set_options ( Array<Key> options ) {
      if ( this.file.get_section ( "options" ) != null ) {
        var section_options = this.file.get_section ( "options" );

        for ( int i = 0; i < options.length; i++ ) {
          section_options.replace_key ( options.index ( i ) );
        }
      }
    }

    public bool set_option ( string key, string val ) {
      bool ret = false;

      if ( this.file.get_section ( "options" ) != null ) {
        KeyWithValue kwv = KeyWithValue.YES;
        if ( val == "" ) {
          kwv = KeyWithValue.NO;
        }

        var option = new Key ( key, val, kwv );

        var section_options = this.file.get_section ( "options" );
        section_options.replace_key ( option );

        for ( int i = 0; i < section_options.get_keys ().length; i++ ) {
          if ( section_options.get_keys ().index ( i ) == option ) {
            ret = true;
            break;
          }
        }
      }

      return ret;
    }

    public bool unset_option ( string key ) {
      bool ret = false;

      if ( this.file.get_section ( "options" ) != null ) {
        var section_options = this.file.get_section ( "options" );
        for ( int i = 0; i < section_options.get_keys ().length; i++ ) {
          var option = section_options.get_keys ().index ( i );
          if ( option.get_key () == key ) {
            option.set_commented ( Commented.YES );
            ret = true;
            break;
          }
        }
      }

      return ret;
    }

    public bool enable_repository ( string repo_name, bool enable ) {
      bool ret = false;
      for ( int i = 0; i < this.get_repositories ().length; i++ ) {
        if ( this.get_repositories ().index ( i ).get_name () == repo_name ) {

          this.get_repositories ().index ( i ).set_commented ( enable ? Commented.NO : Commented.YES );
          ret = true;
          break;
        }
      }

      return ret;
    }

    public bool add_repository ( Section repo ) {
      bool ret = false;
      var repositories = this.get_repositories ();
      var initial_length = repositories.length;

      repositories.append_val ( repo );

      if ( (initial_length + 1) == repositories.length ) {
        this.set_repositories ( repositories );
        ret = true;
      }

      return ret;
    }

    public bool remove_repository ( string repo_name ) {
      bool ret = false;
      var repositories = this.get_repositories ();

      for ( int i = 0; i < repositories.length; i++ ) {
        if ( repositories.index ( i ).get_name () == repo_name ) {
          repositories.remove_index ( i );
          ret = true;
          break;
        }
      }

      this.set_repositories ( repositories );

      return ret;
    }

    public string get_path () {
      return this.file.get_path ();
    }

    public bool changed () {
      return ( this.to_string () != this.original_content );
    }

    public string to_string () {
      return this.file.to_string ();
    }

    public string diff () {
      var original = this.create_temp_file ( this.original_content );
      var modified = this.create_temp_file ( this.to_string () );
      var command = "/usr/bin/diff -u " + original + " " + modified;

      string stdout = "";
      try {
        Process.spawn_command_line_sync ( command, out stdout, null, null );
      } catch ( Error e ) {
        stderr.printf ( "Error showing diff of %s and %s (%s)\n", original, modified, e.message );
      }

      var first_line = stdout.index_of ( '\n'.to_string () );
      var second_line = stdout.index_of ( '\n'.to_string (), first_line + 1 );

      return stdout.slice ( second_line + 1, stdout.length );
    }

    public string create_temp_file ( string contents ) {
      var path = "";
      var file = File.new_for_path ( "/tmp/" + Uuid.string_random () );
      uint8[] data = contents.data;

      try {
        file.replace_contents ( data, null, true, FileCreateFlags.NONE, null, null );
        path = file.get_path ();
      } catch ( Error e ) {
        stderr.printf ( "Error replacing contents of %s (%s)\n", path, e.message );
      }

      return path;
    }
  }
}
