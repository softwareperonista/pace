/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_listbox_fixed.vala
 *
 * Copyright 2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Pace {
  public class ListBoxFixed : ListBox {
    private Gtk.SizeGroup size_group;

    construct {
      this.size_group = new Gtk.SizeGroup ( Gtk.SizeGroupMode.HORIZONTAL );

      this.clicked_button_add_row.connect ( this.on_clicked_button_add_row );
    }

    public new void insert ( Gtk.Widget row, int position ) {
      this.set_size_group ( row as RowSizeGroup );
      base.insert ( row, position );
    }

    public void insert_row_label_entry ( string key, int position, string value = "" ) {
      var row = new RowLabelEntry ( key, value );
      this.insert ( row, position );
    }

    public void insert_row_combobox_entry ( string key, int position, string value = "" ) {
      var row = new RowComboBoxEntry ( key, value );
      row.to_remove.connect ( this.remove_listboxrow );
      this.insert ( row, position );
    }

    public void insert_row_label_label ( string key, int position, string value = "" ) {
      var row = new RowLabelLabel ( key, value );
      this.insert ( row, position );
    }

    public void on_clicked_button_add_row () {
      this.insert_row_combobox_entry ( "Server", -1 );
    }

    private void set_size_group ( RowSizeGroup row ) {
      row.set_size_group ( this.size_group );
    }
  }
}
