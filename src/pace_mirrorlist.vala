/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_mirrorlist.vala
 *
 * Copyright 2019 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Pace {
  public class Mirrorlist {
    private string path;
    private string file;

    public Mirrorlist ( string path ) {
      this.path = path;
      this.set_file_name ( path );
    }

    private void set_file_name ( string path ) {
      string[] path_splited = path.split ( "/" );

      this.file = path_splited[path_splited.length-1];
    }

    public string get_file () {
      return this.file;
    }

    public string get_path () {
      return this.path;
    }
  }
}
