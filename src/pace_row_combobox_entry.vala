/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_row_combobox_entry.vala
 *
 * Copyright 2019 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_row_combobox_entry.ui" )]
  public class RowComboBoxEntry : Gtk.Grid, RowKeyValue, RowSizeGroup {
    [GtkChild]
    private unowned Gtk.ComboBox combobox_keys;
    [GtkChild]
    private unowned Gtk.Entry entry_value;

    public signal void to_remove ( Gtk.ListBoxRow row );

    public RowComboBoxEntry ( string key, string value = "" ) {
      this.set_key ( key );

      this.entry_value.set_text ( value );
    }

    public string get_key () {
      Value key;
      Gtk.TreeIter iter;

      this.combobox_keys.get_active_iter ( out iter );
      this.combobox_keys.get_model ().get_value ( iter, 0, out key );

      return (string)key;
    }

    public string get_value () {
      return this.entry_value.get_text ();
    }

    [GtkCallback]
    private void on_button_delete_clicked () {
      this.to_remove ( this.parent as Gtk.ListBoxRow );
    }

    public void set_key ( string key ) {
      switch ( key ) {
        case "Server":
          this.combobox_keys.set_active ( 0 );
          break;
        case "Include":
          this.combobox_keys.set_active ( 1 );
          break;
        case "SigLevel":
          this.combobox_keys.set_active ( 2 );
          break;
        case "Usage":
          this.combobox_keys.set_active ( 3 );
          break;
      }
    }

    public void set_value ( string value ) {
      this.entry_value.set_text ( value );
    }

    public void set_size_group ( Gtk.SizeGroup size_group ) {
      size_group.add_widget ( this.combobox_keys );
    }
  }
}
