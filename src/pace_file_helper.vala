/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_file_helper.vala
 *
 * Copyright (C) 2019-2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Pace {
  public class FileHelper {
    public static bool write ( string file, string content ) {
      int spawnExitStatus = 0;
      bool root_needed = false;

      try {
        FileUtils.set_contents ( file, content );
      } catch ( Error e ) {
        debug ( "Error creating config file (%s)\n", e.message );
        root_needed = true;
      }

      if ( root_needed ) {
        if ( GLib.Environment.find_program_in_path ( "pkexec" ) != null  ) {
          string command_line = "pkexec pace-file-writer \"" + file + "\" \"" + content + "\"";
          string spawnStdOut;
          string spawnStdError;

          try {
            Process.spawn_command_line_sync ( command_line, out spawnStdOut, out spawnStdError, out spawnExitStatus );
          } catch ( SpawnError spawnCaughtError ) {
            stderr.printf ( "There was an error spawining the process. Details: %s", spawnCaughtError.message );
          }
        } else {
          print ( "You do not have write permission for the file '%s'\n", file );
        }
      }

      return spawnExitStatus == 0;
    }
  }
}
