/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_file_writer.vala
 *
 * Copyright (C) 2018-2019 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

int main ( string[] args ) {
  int exit_code = 0;
  if ( args.length == 3 ) {
    var file = File.new_for_path ( args[1] );
    uint8[] contents = args[2].data;

    try {
      file.replace_contents ( contents, null, true, FileCreateFlags.NONE, null, null );
    } catch ( Error e ) {
      stderr.printf ( "Error replacing contents of %s (%s)\n", args[1], e.message );
      exit_code = 1;
    }
  } else {
    stderr.printf ( "This program requires two parameters\n" );
    exit_code = 1;
  }
  return exit_code;
}
