/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_listbox_repositories.vala
 *
 * Copyright 2018-2019 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using IniParser;

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_listbox_repositories.ui" )]
  public class ListBoxRepositories : Gtk.Grid, DataStatus {
    [GtkChild]
    private unowned ListBoxSortable listbox;
    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned RepositoryForm add_form;
    public string file_path;
    private Array<string> initial_order;

    construct {
      this.add_form.set_buttons_for_add ();
      this.initial_order = new Array<string> ();
      this.file_path = "";
    }

    public Array<Section> get_repositories () {
      int i = 0;
      RowRepository row_repository;
      var repositories = new Array<Section> ();

      var row = this.listbox.get_row_at_index ( i );

      while ( row != null ) {
        row_repository = row.get_child () as RowRepository;

        var repository = row_repository.to_config_repository ();

        repositories.append_val ( repository );

        i++;
        row = this.listbox.get_row_at_index ( i );
      }

      return repositories;
    }

    public void set_repositories ( ConfigFile config_file ) {
      this.listbox.clear ();

      this.file_path = config_file.get_path ();

      var repositories = config_file.get_repositories ();

      for ( int i=0; i < repositories.length; i++ ) {
        var repository = repositories.index ( i );
        var row = this.new_repository ();

        row.set_repository_data ( repository );

        this.listbox.insert ( row, -1 );
        this.initial_order.append_val ( repository.get_name () );
      }
    }

    private void row_to_remove ( RowSortable row ) {
      Gtk.ListBoxRow listbox_row = row.get_parent () as Gtk.ListBoxRow;

      this.listbox.remove_listboxrow ( listbox_row );
    }

    [GtkCallback]
    private void on_row_activated ( Gtk.ListBoxRow row ) {
      RowRepository child = row.get_child () as RowRepository;
      child.on_row_activated ();
    }

    [GtkCallback]
    private void file_changed () {
      this.change ();
    }

    public bool get_changed () {
      bool changed = false;
      int i = 0;
      var row = this.listbox.get_row_at_index ( i );
      Array<string> repository_order = new Array<string> ();

      while ( row != null ) {
        var row_repository = row.get_child () as RowRepository;

        changed = row_repository.get_changed ();

        if ( changed ) {
          break;
        }

        repository_order.append_val ( row_repository.to_config_repository ().get_name () );
        i++;
        row = this.listbox.get_row_at_index ( i );
      }

      if ( !changed ) {
        if ( repository_order.length != this.initial_order.length ) {
          changed = true;
        } else {
          for ( i = 0; i < repository_order.length; i++ ) {
            if ( repository_order.index ( i ) != this.initial_order.index ( i ) ) {
              changed = true;
              break;
            }
          }
        }
      }

      return changed;
    }

    public string get_file_path () {
      return this.file_path;
    }

    [GtkCallback]
    private void on_clicked_button_add_row () {
      this.add_form.insert_row_label_entry ( _("Name" ) );
      this.stack.set_visible_child_name ( "page_add" );
    }

    [GtkCallback]
    private void on_repository_form_cancel () {
      this.stack.set_visible_child_name ( "page_list" );
    }

    [GtkCallback]
    private void on_repository_form_save () {
      var row = this.new_repository ();

      row.set_name ( this.add_form.get_name () );
      row.set_enabled ( true );

      row.set_keys ( this.add_form.get_listbox () );

      this.listbox.insert ( row, -1 );
      this.add_form.clear ();

      this.stack.set_visible_child_name ( "page_list" );
      this.change ();
    }

    private RowRepository new_repository () {
      var row = new RowRepository ();

      row.changed.connect ( this.file_changed );
      row.to_remove.connect ( this.row_to_remove );
      row.drag_over.connect ( this.listbox.clear_row_margins );

      return row;
    }
  }
}
